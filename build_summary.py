#!/usr/bin/env python3
from pathlib import Path
import re

DEFAULT_ENCODING='utf-8'

TARGETFILE = "SUMMARY.md" # name of the file that contains the table of contents

# searches for anything in the same line wrapped by dollar signs
PLACEHOLDER_REGEX = "\$.*\$" 

# matches markdown links and catches
# captures link text in group 1
# captures the uri in group 2
MD_LINK_REGEX = "!?\[([^\]]*)\]\(([^\)]+)\)"
md_rgx = re.compile(MD_LINK_REGEX)

def file_exists(to_check):
    """Returns the given file if it exists. Otherwise raises an Error."""
    if not to_check.exists():
        raise ValueError(to_check.parent + "/" + to_check.name 
            + " couldn't be opened. Please make sure the file exists. " 
            + "Aborting build...")

    return to_check


def replace_md_links(doc_summary):
    """Searches the given text for relative markdown links and appends the folder name. Returns the new text."""
    text = doc_summary.read_text(encoding=DEFAULT_ENCODING)
    md_links = md_rgx.findall(text)  # links are tuples in format (link_text, path)
   
    # filter relative links
    relative_links = [link for link in md_links if Path(str(doc_summary.parent) + "/" + link[1]).exists()]

    for link in relative_links:
        text = text.replace("("+link[1]+")", "("+str(doc_summary.parent)+"/"+link[1]+")")

    return text

def main():
    summary_file = Path(TARGETFILE)
    initial_summary = file_exists(summary_file).read_text(encoding=DEFAULT_ENCODING)

    placeholders = re.findall(PLACEHOLDER_REGEX, initial_summary)

    for placeholder in placeholders:
        # placeholders have the format $directory$
        doc_summary = Path(placeholder[1:-1] + "/" + TARGETFILE)
        if file_exists(doc_summary):
            initial_summary = initial_summary.replace(placeholder, replace_md_links(doc_summary))

    summary_file.write_text(initial_summary, encoding=DEFAULT_ENCODING)


if __name__ == "__main__":
    print("Building summary started.")
    main()
    print("Building summary finished.")
