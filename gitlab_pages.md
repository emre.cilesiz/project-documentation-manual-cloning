# GitLab Pages
# Intro
- Auswahl zwischen vielen Static-Site-Generatoren (SSG), mit verschiedenen Funktionen (Beispiele unter https://gitlab.com/pages)
- CI/CD Pipelines bauen mithilfe dieser Generatoren aus den, in der Repository enthaltenen Dateien, Webseiten auf
- Hosting erfolgt durch GitLab

# Setup
GitLab bietet Beispiele für verschiedene SSGs in eigenen Repositories an. Die Repo des ausgewählt SSG muss geforked werden.
Sobald ein GitLab Runner (egal ob specific oder shared) registriert wurde, kann die CI/CD Pipeline ausgeführt werden. Die build files werden im Ordner `public` erstellt, dessen Inhalt GitLab bereitstellt. Die URL zur gehosteten Seite kann man unter `Settings > Pages` einsehen.


# GitBook
Vorteile:
- keine Anfangskonfiguration nötig
- neben der Dokumentation ist nur die CI/CD Konfigurationsdatei nötig
- viele verschieden Features (Markdown support, Inhaltsverzeichnis, Suche, PDF Export, etc.)

# Limitations
GitLab unterstützt derzeit nur eine GitLab Page pro Repository (siehe [Issue](https://gitlab.com/gitlab-org/gitlab/-/issues/16208)). Mehrere Seiten können nur mit Workarounds erreicht werden.
